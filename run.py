import sys
import argparse
import multiprocessing
import gpuMultiprocessing

# Parsing arguments
parser = argparse.ArgumentParser()
parser.add_argument('--module', required=True,
    help='Specify which module to run. One of {combined, noninformative}')
args = parser.parse_args()

# Choosing which experiment module to run
if args.module == 'noninformative':
    from noninformative.utils import get_command_queue, parse_results
if args.module == 'combined':
    from combined.utils import get_command_queue, parse_results
if args.module == 'nonadaptive':
    from nonadaptive.utils import compute
    compute()
    print('Finished, results are stored in a csv file.')
    sys.exit(0)  # Success

# Instead of GPU, we will just use the assigned CPU (no need to have a GPU)
gpu_id_list = list(range(multiprocessing.cpu_count()))
print('Running using {} CPUs'.format(len(gpu_id_list)))

# Getting the commands
command_queue = get_command_queue()

# Computating the results
print('Initiating the computation of {} experiments.'.format(len(command_queue)))
failed_commands = gpuMultiprocessing.queue_runner(command_queue, gpu_id_list,
                                env_gpu_name='DOES_NOT_MATTER',
                                processes_per_gpu=5, allowed_restarts=2)


if not failed_commands:
    print('Finished, parsing results into a csv file.')
    parse_results()
else:
    print('Finished. Number of failed experiments = {}'.format(len(failed_commands)))
    print('Run parsing of the results into csv file manually.')
    print('Failed commands: ')
    print(failed_commands)
