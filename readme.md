# Group testing simulations

Important links:
[interactive web app](http://group-testing.com/),
[short theoretical background](theory.pdf),
[survey article](https://www.researchgate.net/publication/353976475).

This repository aims to give a quick theoretical and visual overview of the standard group testing methods to anyone who needs to decide which methods are suitable for their situation. The presented methods are computed using the [binGroup R package](https://www.rdocumentation.org/packages/binGroup/versions/2.2-1/) or own Python implementation.

----

## How to use
### Interactive web app [![Webapp](https://gitlab.com/hararticles/group-testing-simulations/uploads/78c7a022bf34165162b4e66e078e24b5/badge_webapp.svg)](http://group-testing.com/)

The easiest way to interact with this repository is to run its web application. You can use it if you want to interactively alter the visualizations from the precomputed results of each module and export the plots in a vector format.

### Running the webapp in Binder [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/hararticles%2Fgroup-testing-simulations/master?urlpath=%2Fvoila%2Frender%2Fwebapp%2Fwebapp.ipynb)
You can lunch the web app also using binder. If the app is overloaded, this may provide faster response. It takes a minute or so until it starts though.

### Running the webapp locally using Docker
We use this for deployment, but naturally, you can also use it to run the webapp locally.
```
git clone https://gitlab.com/hararticles/group-testing-simulations
cd group-testing-simulations
sudo docker build -t group-testing-webapp:latest -f webapp.Dockerfile .
sudo docker run --rm -p 8080:80 group-testing-webapp:latest
# The app is now available at http://0.0.0.0:8080/ or http://localhost:8080/
```
### Running a Jupyter notebook in Binder [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/hararticles%2Fgroup-testing-simulations/master)
If you want more control, you can run the provided jupyter notebooks in Binder. This gives you an access to the code of the visualizations in `visualizations.ipynb`.

### Running a Jupyter notebook with R kernel and binGroup
Furthermore, you can compute characteristics of a certain method for your particular case using the binGroup package through the provided `binGroup.ipynb` with a running R kernel. This notebook will work in an environment with R and R kernel installed, e.g. in the provided Docker. See the section bellow for more info on how to run the Docker container.
You can also use the binGroup package in its official [Shiny web app](https://bilder.shinyapps.io/PooledTesting/) (only standard methods, different visualizations and no precomputed results though).

### Running locally using Docker
In case you want to alter the parameters of the modules and run the parallel computation yourself, the best option is to run this repository in a Docker container on a machine with high number of CPUs. (Does not apply for `nonadaptive` module, which is cheap to compute). The following commands were tested on Ubuntu 16.04 & 18.04 hosts. It may be necessary to alter slightly for Mac or Windows.

- Clone this repository:\
  `git clone https://gitlab.com/hararticles/group-testing-simulations`
- Enter its directory:\
  `cd group-testing-simulations`
- Build the docker image:\
  `docker build -f compute.Dockerfile -t group-testing:latest .`
- Run the container with interactive bash:\
  `docker run --rm -it -v $PWD:/home/docker -u $(id -u):$(id -g) -p 8080:8080 group-testing:latest`
  - To start the computation of specific modules:
    `python3 run.py --module=noninformative` \
    `python3 run.py --module=combined` \
    `python3 run.py --module=nonadaptive`
  - To run the notebooks:
    `jupyter notebook --ip=0.0.0.0 --port=8080 --allow-root --no-browser`
  - To run the web app:
    `voila webapp/webapp.ipynb --port=8080 --no-browser`

----

## Contents of the repository
- `run.py` script for running the computation of one of the modules
- `visualize.ipynb` plotting the results of each module
- `binGroup.ipynb` quick access to R kernel with pre-installed binGroup package

**Modules** (with precomputed results):
- `noninformative`\
   Computes the characteristics of non-informative methods D2, D3, A2 by running the R code from binGroup package in parallel on all CPUs. Allows to alter the parameters specified in the `get_command_queue()` in `noninformative/utils.py` and outputs .json files. These are later converted into a structured .csv file using the `parse_results()` which serves as a base for the visualizations of the *optimal group size over infection rate with respect to expected number of tests per person*. More information in Section 1.2 of the [theoretical background](theory.pdf). Precomputed results on a coarse grid are available in `noninformative/results/results.csv`.
- `combined`\
   Computes the characteristics of informative methods on a group created by combining samples from high-risk and low-risk individuals on a grid of parameters specified in `get_command_queue()` in `combined/utils.py` and outputs .json files. These are later converted into a structured .csv file using the `parse_results()` which serves as a base for the visualizations of the *comparison of combined vs. separate group testing*. More information in Section 1.3.1 of the [theoretical background](theory.pdf). Precomputed results on a coarse grid are available in `combined/results/results.csv`.
- `nonadaptive`\
   Computes the characteristics of the non-adaptive A1 method. Allows to alter the parameters specified in `nonadaptive/utils.py` and outputs a .csv file which serves as a base for the visualizations. These show the *trade-off between the set parameters and various metrics*. More information in Section 1.3.2 of the [theoretical background](theory.pdf). Precomputed results are available in `nonadaptive/results/results.csv`.

----

## Authorship & Acknowledgement
This repository, the document containing additional theoretical information and the survey article are a joint effort of (in alphabetical order): Julius Berner<sup>1</sup>, Dennis Elbrachter<sup>1</sup>, David Fischer<sup>2</sup>, Tim Fuchs<sup>3</sup>, Philipp Grohs<sup>1,4,5</sup>, Pavol Harar<sup>4,6</sup>, Felix Krahmer<sup>3</sup>, Fabian J. Theis<sup>2,3</sup> and Claudio Verdun<sup>3,7</sup>

1) Faculty of Mathematics, University of Vienna
2) Institute of Computational Biology, Helmholtz Zentrum München
3) Department of Mathematics, Technical University of Munich
4) Research Platform Data Science, University of Vienna
5) Johann Radon Institute for Computational and Applied Mathematics, Austrian Academy of Sciences
6) Department of Telecommunications, Brno University of Technology
7) Department of Electrical and Computer Engineering, Technical University of Munich

Computational resources were granted by the Research Network Data Science, University of Vienna.


### Citation
```
@article{verdun2021group,
  title={Group testing for SARS-CoV-2 allows for up to 10-fold efficiency increase across realistic scenarios and testing strategies},
  author={Verdun, Claudio M and Fuchs, Tim and Harar, Pavol and Elbr{\"a}chter, Dennis and Fischer, David S and Berner, Julius and Grohs, Philipp and Theis, Fabian J and Krahmer, Felix},
  journal={Frontiers in Public Health},
  pages={1205},
  year={2021},
  publisher={Frontiers},
  doi={10.3389/fpubh.2021.583377},
  note={medRxiv preprint at \href{https://doi.org/10.1101/2020.04.30.20085290}{10.1101/2020.04.30.20085290}},
}
```

### Contact
Regarding the repository, please contact its owner. Regarding the survey article, please contact its corresponding authors.

[theory]: theory.pdf
