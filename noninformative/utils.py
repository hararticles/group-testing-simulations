import random
import os, json
import itertools
import numpy as np
import pandas as pd
import seaborn as sns
from p_tqdm import p_umap
from matplotlib import pyplot as plt


# Configuring paths
module_dir = os.path.dirname(os.path.abspath(__file__))
script_path = os.path.join(module_dir, 'compute.r')
results_dir = os.path.join(module_dir, 'results')
jsons_dir = os.path.join(module_dir, results_dir, 'jsons')
csv_path = os.path.join(results_dir, 'results.csv')

# Configuring which experiments are going to be run
# This module can also compute informative methods assuming the user
# wants to use the default beta distribution altering only the alpha value
def get_command_queue():

    # Creating the direcory for the output json files
    os.makedirs(jsons_dir, exist_ok=True)

    # Hyperparameter grid
    PROBA = np.logspace(0, -4, num=30, endpoint=True, base=np.e)/5
    GROUP = np.arange(3,41)
    N_ALGO = ['D2', 'D3', 'A2']
    SE = [1.0, 0.95, 0.90]
    SP = [1.0, 0.95, 0.90]

    I_ALGO = [] # ['ID2', 'ID3', 'IA2']
    I_SEED = np.arange(100) # N of trials with different seed
    I_ALPHA = [10.0]

    # Different hp for noninformative and informative
    N_grid = list(itertools.product(N_ALGO, PROBA, GROUP, SE, SP))
    I_grid = list(itertools.product(I_ALGO, PROBA, GROUP, I_SEED, I_ALPHA))

    # Building the command template
    flags = 'PYTHONHASHSEED=0'
    N_hp_args = '--algo="{}" --proba={} --group={} --se={} --sp={}'
    I_hp_args = ' '.join([N_hp_args, '--seed={} --alpha={}'])
    N_command = ' '.join([flags, 'Rscript {} --metrics="ET" --outdir="{}"'.format(script_path, jsons_dir), N_hp_args])
    I_command = ' '.join([flags, 'Rscript {} --metrics="ET" --outdir="{}"'.format(script_path, jsons_dir), I_hp_args])

    # Building the command queue
    N_command_queue = [N_command.format(*x) for x in N_grid]
    I_command_queue = [I_command.format(*x) for x in I_grid]
    command_queue = N_command_queue + I_command_queue
    random.shuffle(command_queue)

    return command_queue


def parse_results():
    # Parsing an individual json results file
    # File name convention: algo_proba_group_seed_alpha.json or algo_proba_group_seed_.json
    files = os.listdir(jsons_dir)
    to_process = len(files)

    def get_values(file):
        split = os.path.splitext(file)[0].split('_')
        with open(os.path.join(jsons_dir, file), 'r') as f:
            j = json.load(f)
        opt = j['opt.ET']
        alpha = float(split[6]) if split[6] != '' else np.nan
        return pd.Series({'algo': split[0], 'alpha': alpha, 'seed': split[5], # Group by these
                          'prob': split[1], 'group': split[2], # Sort by these
                          'value': opt['value'], 'pse': opt['PSe'], 'psp': opt['PSp'], # Use the rest
                          'base_se': j['Se'], 'base_sp': j['Sp'],
                          'ppv': opt['PPPV'], 'npv': opt['PNPV'],
                          'stage2': opt.get('OTC').get('Stage2')})

    series = p_umap(get_values, files)
    df = pd.concat(series, axis=1).T

    # Saving the structured results into a file
    df.to_csv(csv_path, index=False)


def load_csv():
    # Loading the results back from a file
    df = pd.read_csv(csv_path) # Loading from the file - redundant

    # In order to use groupby with np.nan, we need to convert alpha to string
    columns = ['algo', 'alpha', 'seed', 'prob', 'group', 'value',  'pse',
               'psp', 'base_se', 'base_sp', 'ppv', 'npv', 'stage2']
    dtypes = [np.str, np.str, np.int, np.float, np.int, np.float, np.float,
              np.float, np.float, np.float, np.float, np.float, np.dtype('O')]

    # Converting into desired dtypes
    for col, dtype in zip(columns, dtypes):
        df[col] = df[col].astype(dtype)
    return df


def get_plot_data(df):
    # Df is expected to be filtered for just one base_se and one base_sp
    # Unique vectors of points
    algo_points = df['algo'].unique()
    alpha_points = np.sort(df['alpha'].unique().astype(np.float)).astype(np.str)
    seed_points = np.sort(df['seed'].unique())
    proba_points = np.sort(df['prob'].unique())
    group_points = np.sort(df['group'].unique())

    def get_heat_matrix(group, key):
        # First sort according to y and then x axis in the final plot
        presorted = np.array(group.sort_values(by=['group', 'prob'])[key])
        return presorted.reshape(len(group_points), len(proba_points))

    # Building the data_for_plots dict with structure:
    # {algo: {alpha: {seed: {ets: ets_mat, pse: pse_mat, psp: psp_mat}}}}
    print('Building the data for plotting.')
    data_for_plots = {algo: {alpha: {} for alpha in alpha_points} for algo in algo_points}
    gr_algo = df.groupby('algo')
    for algo in algo_points:
        gr_alpha = gr_algo.get_group(algo).groupby('alpha')
        for alpha in alpha_points:
            if alpha in gr_alpha.groups.keys():
                gr_seed = gr_alpha.get_group(alpha).groupby('seed')
                for seed in seed_points:
                    if seed in gr_seed.groups.keys():
                        group = gr_seed.get_group(seed)
                        ets_mat = get_heat_matrix(group, 'value')
                        pse_mat = get_heat_matrix(group, 'pse')
                        psp_mat = get_heat_matrix(group, 'psp')
                        data_for_plots[algo][alpha][seed] = {'ets': ets_mat, 'pse': pse_mat, 'psp': psp_mat}
    print('Finished.')
    return proba_points, group_points, data_for_plots


def heat(prob, group, matrix, figsize=(18,20), dpi=100, xlim=None, ylim=None, clim=None,
         mask=None, plot_heatmap=True, plot_optima=True, optima_step=2, obj='min',
         title="Optimal group size over infection rate with respect to the metric.",
         subtitle="", xlabel="p [%]", ylabel="k (initial)", zlabel="E",
         fig=None, ax=None, cax=None, cmap='coolwarm'):

    if fig is None:
        plt.figure(figsize=figsize, dpi=dpi)
    ax = plt.gca() if ax is None else ax
    ax.set_title('\n'.join([title, subtitle]))


    if plot_optima:
        optima = matrix.argmin(axis=0) if obj == 'min' else matrix.argmax(axis=0)
        x = np.arange(len(optima)) + 0.5
        y = optima + 0.5
        ax.plot(x, y, '.-', linewidth=1., color='black', markersize = 0, label='E of optimal k')
        ax.plot(x[::optima_step], y[::optima_step], '.', markersize = 6.0, linestyle='None', color='black', label=None)

        for i in range(0, len(optima), optima_step):
            label = "{:.2f}".format(matrix[optima[i], i])
            ax.annotate(label, (i+0.5,optima[i]+0.5), textcoords="offset points",
                         xytext=(12,6), ha='center', fontsize='smaller') #fontsize=12

    if plot_heatmap:
        if clim:
            clim_mask = np.zeros_like(matrix).astype(np.bool)
            clim_mask[matrix < clim[0]] = True  # Remove too small values
            clim_mask[matrix > clim[1]] = True  # Remove too big values
        else:
            clim_mask, clim = np.zeros_like(matrix).astype(np.bool), (None, None)

        # Merge masks
        mask = clim_mask if mask is None else mask | clim_mask

        ax = sns.heatmap(matrix, yticklabels=group, cmap=cmap,
                              mask=mask, vmin=clim[0], vmax=clim[1],
                              xticklabels=['{:.2f}'.format(100*x) for x in prob],
                              # linewidths=0.5, linecolor='white',
                              cbar_kws={'label': zlabel}, ax=ax, cbar_ax=cax)
        ax.invert_yaxis()
        ax.set_yticklabels(ax.get_yticklabels(), rotation = 0) # fontsize = 12
        ax.set_frame_on(True)

        # Borders on ax and cax
        if cax is not None:
            cax.set_frame_on(True)
        for _, spine in ax.spines.items():
            spine.set_visible(True)

    def format_coord(x, y):
        xi, yi = np.floor(x).astype(np.int), np.floor(y).astype(np.int)
        return 'p: {:.2f},  k: {:.0f}, E: {:.3f}'.format(
                prob[xi]*100, group[yi], matrix[yi][xi])

    ax.format_coord = format_coord
    ax.legend()
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)
    return plt


def plot_ets_pse_psp(proba_points, group_points, data_for_plots, base_se, base_sp,
                     figsize=(18,12), dpi=100, show=True, save=True, title=True):
    lexicon = {'ets': 'E', 'pse': 'Se', 'psp': 'Sp'}

    # ets, pse, psp heatmaps per each algo-alpha with seed axis averaged)
    for algo, alphas in data_for_plots.items():
        for alpha, seeds in alphas.items():
            for kind in ['ets', 'pse', 'psp']:
                mats = [matrices[kind] for seed, matrices in seeds.items()]
                if mats:
                    matrix = np.squeeze(np.mean(mats, axis=0))

                    title = ('Optimal group size over infection rate w.r.t. {}({})\n'
                             '(base test Se={}, Sp={})').format(lexicon[kind], algo, \
                              base_se, base_sp) if title else ''

                    c = {'ets': {'obj': 'min', 'clim': (0.00, 1.00), 'cmap': 'coolwarm'},
                         'pse': {'obj': 'max', 'clim': None, 'cmap': 'coolwarm_r'},
                         'psp': {'obj': 'max', 'clim': None, 'cmap': 'coolwarm_r'}}


                    plot = heat(proba_points, group_points, matrix, figsize=figsize,
                                xlim=None, ylim=None, clim=c[kind]['clim'], dpi=dpi,
                                plot_heatmap=True, plot_optima=True, optima_step=2,
                                obj=c[kind]['obj'], title=title, subtitle='',
                                cmap=c[kind]['cmap'])

                    fname = '{}({})_k_{}_base_se_{}_base_sp_{}'.format(lexicon[kind], \
                            algo, np.max(group_points), base_se, base_sp)
                    if alpha != 'nan':
                        fname += '_{}'.format(alpha)
                    if save:
                        plot.savefig(os.path.join(results_dir, '{}.pdf'.format(fname)),
                                     bbox_inches = 'tight', pad_inches = 0)
                    if show:
                        plot.show()
                    plot.close()
                else:
                    print('No data for: algo:{} alpha:{} kind:{}'.format(algo, alpha, kind))


def plot_ets_masked(proba_points, group_points, data_for_plots, base_se, base_sp,
                    sensitivity_min, specificity_min, show=True, save=True,
                    figsize=(18,12), dpi=100, title=True, fig=None, ax=None,
                    cax=None, specific_algo=None):
    # ets heatmaps per each algo-alpha with seed axis averaged masked with pse and psp)
    for algo, alphas in data_for_plots.items():
        if specific_algo is not None:
            if algo != specific_algo:
                continue
        for alpha, seeds in alphas.items():
            matrix_dict = {}
            for kind in ['ets', 'pse', 'psp']:
                mats = [matrices[kind] for seed, matrices in seeds.items()]
                if mats:
                    matrix = np.squeeze(np.mean(mats, axis=0))
                    matrix_dict[kind] = matrix
                else:
                    matrix_dict[kind] = []
                    print('No data for: algo:{} alpha:{} kind:{}'.format(algo, alpha, kind))

            title = ('Optimal group size over infection rate w.r.t. E({})\n'
                     '(Masked where Se<{}, Sp<{} and E>1.0, base test Se={}, '
                     'Sp={})').format(algo, sensitivity_min, specificity_min, \
                      base_se, base_sp) if title else ''

            mask = (matrix_dict['pse'] <= sensitivity_min) | (matrix_dict['psp'] <= specificity_min)

            plot = heat(proba_points, group_points, matrix_dict['ets'], figsize=figsize,
                        xlim=None, ylim=None, clim=(0.00, 1.00), mask=mask, dpi=dpi,
                        plot_heatmap=True, plot_optima=True, optima_step=2,
                        obj='min', title=title, subtitle='', fig=fig, ax=ax, cax=cax)

            fname = 'E({})_k_{}_Se<{}_Sp<{}_base_se_{}_base_sp_{}'.format(algo, \
                     np.max(group_points), sensitivity_min, specificity_min, base_se, base_sp)
            if alpha != 'nan':
                fname += '_{}'.format(alpha)
            if save:
                plot.savefig(os.path.join(results_dir, '{}.pdf'.format(fname)),
                            bbox_inches = 'tight', pad_inches = 0)
            if show:
                plot.show()
