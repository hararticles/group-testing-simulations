library("optparse")
library("binGroup")
library("jsonlite")

# Parse the command line arguments
option_list = list(
    make_option(c("-a", "--algo"), type="character", default=NULL, help="Algorithm to use, one of D2, D3, ID2, ID3, A2, IA2, A2M."),
    make_option(c("-r", "--se"), type="numeric", default=0.98, help="Sensitivity, also called recall of the testing method."),
    make_option(c("-s", "--sp"), type="numeric", default=0.9988, help="Specificity, also called selectivity of the testing method."),
    make_option(c("-p", "--proba"), type="numeric", default=NULL, help="Probability of an individual being infected."),
    make_option(c("-g", "--group"), type="integer", default=NULL, help="Size of the group being tested."),
    make_option(c("-m", "--metrics"), type="character", default=NULL, help="Comma separated list of evaluation metrics (objective funcs if optimizing), e.g. 'ET,MAR,GR'."),
    make_option(c("-w", "--d1"), type="numeric", default=1, help="Weight of misclassified negatives in GR metric."),
    make_option(c("-v", "--d2"), type="numeric", default=1, help="Weight of misclassified positives in GR metric."),
    make_option(c("-l", "--alpha"), type="numeric", default=NULL, help="Alpha of the beta distribution for informative methods."),
    make_option(c("-t", "--seed"), type="integer", default=1, help="Seed for the random beta distribution for informative methods."),
    make_option(c("-o", "--outdir"), type="character", default="jsons", help="Directory where the output json files will be stored.")
);
opt_parser = OptionParser(option_list=option_list);
opt = parse_args(opt_parser);

dir.create(opt[["outdir"]], showWarnings=FALSE)
fname <- file.path(opt[["outdir"]], paste(opt[["algo"]], opt[["proba"]], opt[["group"]], opt[["se"]], opt[["sp"]], opt[["seed"]], opt[["alpha"]], sep="_"))

# Compute the characteristics
set.seed(opt[["seed"]])
results <- OTC(algorithm=opt[["algo"]], p=opt[["proba"]], Se=opt[["se"]], Sp=opt[["sp"]], group.sz=opt[["group"]], obj.fn=c(strsplit(opt[["metrics"]], ",")[[1]]), weights=c(opt[["d1"]], opt[["d2"]]), alpha=opt[["alpha"]])

# Save the results to a json file
write(toJSON(results, auto_unbox=TRUE, pretty=TRUE, digits=NA), file=paste(fname, '.json', sep=''))
print('Finished. Results saved to a json file.')
