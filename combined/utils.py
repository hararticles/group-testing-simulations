import random
import os, json
import itertools
import numpy as np
import pandas as pd
import seaborn as sns
from p_tqdm import p_umap
from matplotlib import pyplot as plt


# Configuring pathsCombined vs. separate group testing
module_dir = os.path.dirname(os.path.abspath(__file__))
script_path = os.path.join(module_dir, 'compute.r')
results_dir = os.path.join(module_dir, 'results')
jsons_dir = os.path.join(module_dir, results_dir, 'jsons')
csv_path = os.path.join(results_dir, 'results.csv')


# Configuring which experiments are going to be run
def get_command_queue():

    # Creating the direcory for the output json files
    os.makedirs(jsons_dir, exist_ok=True)

    # Hyperparameter grid
    ALGO = ['ID2', 'ID3', 'IA2']
    HIGH_P = [0.05, 0.10, 0.15, 0.20, 0.25]
    LOW_P = np.ceil(np.logspace(0, -4, num=10, endpoint=True, base=np.e)*500)/10000
    GROUP_SIZE = np.arange(3,41)
    SE = [1.0, 0.95, 0.9]
    SP = [1.0, 0.95, 0.9]
    grid = list(itertools.product(ALGO, HIGH_P, LOW_P, GROUP_SIZE, SE, SP))

    # Building the command template
    flags = 'PYTHONHASHSEED=0'
    hp_args = '--algo="{}" --high_p={} --low_p={} --group={} --se={} --sp={}'
    command = ' '.join([flags, 'Rscript {} --outdir="{}"'.format(script_path, jsons_dir), hp_args])

    # Building the command queue
    command_queue = [command.format(*x) for x in grid]
    random.shuffle(command_queue)
    return command_queue


def parse_results():
    # Parsing an individual json results file
    # File name convention: Algo-IA2-groupsize-3-high_p-0.1-low_p-0.01-Se-0.98-Sp-0.9988-Maxh-1.json
    files = os.listdir(jsons_dir)
    to_process = len(files)

    def get_values(file):
        split = os.path.splitext(file)[0].split('-')
        algo = split[1]
        group_size = split[3]
        high_p = split[5]
        low_p = split[7]
        base_se = split[9]
        base_sp = split[11]
        max_h = split[13]

        with open(os.path.join(jsons_dir, file), 'r') as f:
            j = json.load(f)

        rows = [ # list of nested dicts each of which is going to be one csv row
            {(algo, int(group_size), int(high_p_elems), float(high_p),
             float(low_p), float(base_se), float(base_sp)):
             {'E(T)': r['E(T)'], 'se': r['se'], 'sp': r['sp'],
             'ppv': r['ppv'], 'npv': r['npv'], 'max_h': max_h, 'type': r['type'],
             'stage2': r['stage2'] if r['stage2'] else None,
             'poolsizes': r['poolsizes'] if r['poolsizes'] else None}}
            for high_p_elems, r in j[algo][group_size].items()]
        return rows

    # Merging the jsons to one big dict
    dicts_list = p_umap(get_values, files)
    dicts_list = list(itertools.chain(*dicts_list)) # Flatten
    dicts = dict()
    [dicts.update(x) for x in dicts_list]

    # Creating a multi-index dataframe
    df = pd.DataFrame(dicts).T
    index_names = ['algo', 'group_size', 'high_p_elems',
                   'high_p', 'low_p', 'base_se', 'base_sp']
    df.index.set_names(index_names, inplace=True)

    # Saving the structured results into a file
    df.to_csv(csv_path, index=True)


def load_csv():
    # Loading the results from a csv file
    usecols = ['algo', 'group_size', 'high_p_elems', 'high_p', 'low_p', 'E(T)',
               'base_se', 'base_sp', 'se', 'sp', 'type']
    df = pd.read_csv(csv_path, usecols=usecols)
    df = df.sort_index()
    return df


def get_optima_idx(df, _type):
    """Returns indices of points with min E(T) of points of the _type."""
    x = df.loc[df['type'] == _type] # One of 'MIX', 'LOW', 'HIGH'
    x = x.groupby(['algo', 'high_p', 'low_p'])['E(T)'].idxmin()
    idx = pd.DataFrame(x)
    idx.reset_index(inplace=True)
    return idx['E(T)']


def get_optima_data(grouped):
    """Works on a grouped data frame and returns a comparison of combined
    vs. separate testing along with interesting data, e.g. optmal group size"""
    low = grouped.loc[grouped['type'] == 'LOW']['E(T)'].iloc[0]
    mix = grouped.loc[grouped['type'] == 'MIX']['E(T)'].iloc[0]
    k = grouped.loc[grouped['type'] == 'MIX']['group_size'].iloc[0]
    k_l = grouped.loc[grouped['type'] == 'LOW']['group_size'].iloc[0]
    k_h = grouped.loc[grouped['type'] == 'HIGH']['group_size'].iloc[0]
    h = grouped.loc[grouped['type'] == 'MIX']['high_p_elems'].iloc[0]
    high = grouped.loc[grouped['type'] == 'HIGH']['E(T)'].iloc[0]

    # Baseline
    sep = (min(h, h * high) + (k - h) * low) / k

    # Comparison
    saved = round((1 - (mix / sep))*1e5)/1e5

    # Annotations for the plot
    if saved > 0:
        annot = 'h={}\nk={}\nc={:.3f}\ns={:.3f}'.format(h, k, mix, sep)
    else:
        annot = 'k$_{{l}}$={}\nk$_{{h}}$={}'.format(k_l, k_h)

    return pd.Series([k, h, mix, k_l, k_h, sep, saved, annot],
                     index=['k', 'h', 'c', 'k_l', 'k_h', 's', 'saved', 'annot'])


def get_plot_data(df):
    df['optima'] = False
    df.loc[get_optima_idx(df, 'MIX'), 'optima'] = True
    df.loc[get_optima_idx(df, 'LOW'), 'optima'] = True
    df.loc[get_optima_idx(df, 'HIGH'), 'optima'] = True
    df = df.loc[df['optima']]
    gr = df.groupby(['algo', 'high_p', 'low_p'])
    return gr.apply(get_optima_data)


# Plotting
def plot_combi_vs_sep(optimal_df, max_k, base_se, base_sp, title=True,
                      show=True, save=True, figsize=(18,8),
                      fig=None, ax=None, cax=None, dpi=100,
                      specific_algo=None, legend=False):

    for algo in optimal_df.index.unique(level='algo'):
        if specific_algo is not None:
            if algo != specific_algo:
                continue
        df = optimal_df.loc[algo]
        pivot = pd.pivot_table(df, values=['saved'], columns=['low_p'], index=['high_p'])
        annot = df['annot'].unstack()

        # Setting up the figure
        if fig is None:
            plt.figure(figsize=figsize, dpi=dpi)
            fig = plt.gcf()
        ax = plt.gca() if ax is None else ax
        cbar_kws={'fraction': 0.03, 'pad': 0.01} if cax is None else {}
        cbar_kws.update({'label': 'Relative decrease [%] in E'})

        if title:
            ax.set_title(('Combined vs. separate group testing\n'
                          '({} constrained by group_size={}, base test Se={}, Sp={})'.format(
                          algo, max_k, base_se, base_sp)))
        if legend:
            t = ('h = high-risk individuals per group, k / k$_{{l}}$ / k$_{{h}}$ = optimal '
                 'group size for combined / separate-low / separate-high testing,\n'
                 'c / s = expected number of tests per person for '
                 'combined / separate testing respectively')
            fig.text(0.5, -0.05, t, ha='center') #wrap=True


        yticklabels=['{:.0f}'.format(x*100) for x in pivot['saved'].index]
        xticklabels=['{:.2f}'.format(x*100) for x in pivot['saved'].columns]
        values = pivot['saved'].values*100
        vmax = 1 if np.max(values) <= 0 else np.max(values)
        ax = sns.heatmap(values, annot=annot, fmt="", mask=None,
                         yticklabels=yticklabels, xticklabels=xticklabels,
                         vmin=0.0, vmax=vmax,
                         cmap='Greens', linewidths=0.5, linecolor='black', ax=ax,
                         cbar_ax=cax, cbar_kws=cbar_kws, annot_kws={"size": 'smaller'})

        # Hatch overlay
        plt.rcParams['hatch.linewidth'] = 0.3
        mask = np.ma.masked_greater(pivot['saved'].values, 0)
        ax.pcolor(np.arange(len(pivot['saved'].columns)+1),
                   np.arange(len(pivot['saved'].index)+1),
                   mask, hatch='/', alpha=0.0)

        # Formating the interactive plot on hover coordinates
        def format_coord(x, y):
            xi, yi = np.floor(x).astype(np.int), np.floor(y).astype(np.int)
            return 'p (high-risk): {}%,  p (low-risk): {}%, \
                    Relative decrease in E: {:.3f}%'.format(
                    yticklabels[yi], xticklabels[xi],
                    pivot['saved'].values[yi][xi]*100)
        ax.format_coord = format_coord

        ax.invert_yaxis()
        ax.set_yticklabels(ax.get_yticklabels(), rotation = 0)
        ax.set_ylabel('p [%] of a high-risk individual')
        ax.set_xlabel('p [%] of a low-risk individual')

        # Borders on ax and cax
        if cax is not None:
            cax.set_frame_on(True)
        for _, spine in ax.spines.items():
            spine.set_visible(True)

        if save:
            fname = 'combi({})_maxk_{}'.format(algo, max_k)
            plt.savefig(os.path.join(results_dir, '{}.pdf'.format(fname)),
                        bbox_inches = 'tight', pad_inches = 0)
        if show:
            plt.show()

        fig, ax = None, None
