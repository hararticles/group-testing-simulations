library("optparse")
library("binGroup")
library("jsonlite")

# Parse the command line arguments
option_list = list(
    make_option(c("-a", "--algo"), type="character", default=NULL,
    help="Algorithm to use, one of D2, D3, ID2, ID3, A2, IA2, A2M."),

    make_option(c("-i", "--high_p"), type="numeric", default=NULL,
    help="Probability of infection of high-risk indivitual."),

    make_option(c("-l", "--low_p"), type="numeric", default=NULL,
    help="Probability of infection of low-risk individual."),

    make_option(c("-r", "--se"), type="numeric", default=0.98,
    help="Sensitivity, also called recall of the testing method."),

    make_option(c("-s", "--sp"), type="numeric", default=0.9988,
    help="Specificity, also called selectivity of the testing method."),

    make_option(c("-g", "--group"), type="integer", default=NULL,
    help="Size of the group being tested."),

    make_option(c("-m", "--maxh"), type="integer", default=1,
    help="Maximal number of high-risk individuals in the group."),

    make_option(c("-o", "--outdir"), type="character", default="jsons",
    help="Directory where the output json files will be stored.")
);
opt_parser = OptionParser(option_list=option_list);
opt = parse_args(opt_parser);

# Checks for correctness of maxh
if (opt[['maxh']] > opt[['group']]){opt[['maxh']] <- opt[['group']]-1}
if (opt[['maxh']] < 0){opt[['maxh']] <- 0}

# Preparing the output file path
dir.create(opt[["outdir"]], showWarnings=FALSE)
funcs_I <- list('ID3' = Inf.D3, 'ID2' = Inf.Dorf, 'IA2' = Inf.Array)
funcs_N <- list('ID3' = NI.D3, 'ID2' = NI.Dorf, 'IA2' = NI.Array)
fname <- file.path(opt[["outdir"]],
                   paste('Algo', opt[["algo"]], 'groupsize', opt[["group"]],
                   'high_p', opt[["high_p"]], 'low_p', opt[["low_p"]],
                   'Se', opt[["se"]], 'Sp', opt[["sp"]], 'Maxh', opt[["maxh"]], sep='-'))

# Computing E(T) of group of low-risk individuals with noninformative method
# Computing E(T) of combined group with informative method
# Computing E(T) of group of high-risk individuals with noninformative method
results <- list()
for (i in c(0:opt[['maxh']], opt[['group']])){
    p_vector <- c(rep(opt[["low_p"]], opt[["group"]]))
    p_vector[c(0:i)] <- opt[["high_p"]]

    high_in_group <- i
    low_in_group <- opt[["group"]] - i

    # Deciding between noninformative and informative methods
    # There seems to be a bug in binGroup when running p=0.25 and group = 10 or 7
    # with an informative D2, that is why here we use noninformative.
    if (i == 0){
        funcs <- funcs_N
        type <- 'LOW'
    } else if (i == opt[['group']]) {
        funcs <- funcs_N
        type <- 'HIGH'
    } else {
        funcs <- funcs_I
        type <- 'MIX'
    }

    out <- funcs[[opt[["algo"]]]](p=p_vector, Se= opt[["se"]], Sp= opt[["sp"]],
                                    group.sz=opt[["group"]], obj.fn='ET')

    results[[opt[["algo"]]]][[as.character(opt[["group"]])]][[as.character(i)]] <-
        list('E(T)' = out[['opt.ET']][["value"]],
             'se' = out[['opt.ET']][["PSe"]],
             'sp' = out[['opt.ET']][["PSp"]],
             'ppv' = out[['opt.ET']][["PPPV"]],
             'npv' = out[['opt.ET']][["PNPV"]],
             'stage2' = out[['opt.ET']][['OTC']][["Stage2"]],
             'poolsizes' = out[['opt.ET']][['OTC']][["pool.szs"]],
             'type' = type)
}

write(toJSON(results, auto_unbox=TRUE, pretty=TRUE, digits=NA), file=paste(fname, '.json', sep=''))
print('Finished. Results saved to a json file.')
