FROM python:3.8.15-slim
WORKDIR /app
COPY requirements.txt /requirements.txt
RUN pip install `head -n-1 /requirements.txt`
RUN jupyter nbextension install --py --sys-prefix widgetsnbextension && \
    jupyter nbextension enable --py --sys-prefix widgetsnbextension

COPY webapp/webapp.ipynb webapp/
COPY combined/utils.py combined/
COPY combined/results/results.csv combined/results/
COPY nonadaptive/utils.py nonadaptive/
COPY nonadaptive/results/results.csv nonadaptive/results/
COPY noninformative/utils.py noninformative/
COPY noninformative/results/results.csv noninformative/results/

CMD ["voila", "webapp/webapp.ipynb", "--no-browser", "--port=80", "--Voila.ip=0.0.0.0", "--enable_nbextensions=True", "--MappingKernelManager.cull_interval=60", "--MappingKernelManager.cull_idle_timeout=150"]
