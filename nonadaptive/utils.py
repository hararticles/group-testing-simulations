import os
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

# Configuring pathsCombined vs. separate group testing
module_dir = os.path.dirname(os.path.abspath(__file__))
results_dir = os.path.join(module_dir, 'results')
csv_path = os.path.join(results_dir, 'results.csv')

# Hyperparameters
P = np.logspace(-3, -1, num=8)
GROUP_SIZE = np.arange(3,41)
DIVISIBILITY = np.arange(2, 5)
SE = np.array([1.0, 0.95, 0.90])
SP = np.array([1.0, 0.95, 0.90])

group_size, divisibility, p, se, sp = np.meshgrid(GROUP_SIZE, DIVISIBILITY, P, SE, SP)
F = {
    'group_sz': group_size.flatten(),
    'divisibility': divisibility.flatten(),
    'p': p.flatten(),
    'sensitivity': se.flatten(),
    'specificity': sp.flatten()
 }

# non-adaptive array-testing tests/person
def nonadapt_ETP(F):
    return F['divisibility']/F['group_sz']

# non-adaptive array-testing overall sensitivity
def nonadapt_sensitivity(F):
    return F['sensitivity']**F['divisibility']

# non-adaptive array-testing overall specificity
def nonadapt_specificity(F):
    p_all_pos = ((1-(1-F['p'])**(F['group_sz']-1))*F['sensitivity'] + (1-F['p'])**(F['group_sz']-1)*(1-F['specificity']))**F['divisibility']
    return 1-p_all_pos

# non-adaptive accuracy
def nonadapt_acc(F):
    return F['p']*nonadapt_sensitivity(F)+(1-F['p'])*nonadapt_specificity(F)

# non-adaptive MAR - the expected number of tests divided by the expected number of correct classifications
# described in Malinovsky et al. (2016), see also http://chrisbilder.com/grouptesting/HBTM/paperMay2019.pdf)
def nonadapt_MAR(F):
    return nonadapt_ETP(F)/nonadapt_acc(F)

# non-adaptive GR - a linear combination of the expected number of tests, the number of misclassified negatives, and the number of misclassified positives
# described in Graff & Roeloffs (1972) (here per person)
def nonadapt_GR(F, reg1=3., reg2=3.):
    return nonadapt_ETP(F) + reg1*(1-F['p'])*(1-nonadapt_specificity(F)) + reg2*F['p']*(1-nonadapt_sensitivity(F))

# Positive predictive value - 'number of actual positives'/'number of declared positives'
def nonadapt_PPV(F):
    return F['p']*nonadapt_sensitivity(F)/(F['p']*nonadapt_sensitivity(F)+(1-F['p'])*(1-nonadapt_specificity(F)))

def compute():
    data_dict = {'k': F['group_sz'],
                 'n': F['divisibility'],
                 'p': F['p'],
                 'E': nonadapt_ETP(F),
                 'Se': nonadapt_sensitivity(F),
                 'Sp': nonadapt_specificity(F),
                 'PPV': nonadapt_PPV(F),
                 'ACC': nonadapt_acc(F),
#                  'MAR': nonadapt_MAR(F),
#                  'GR': nonadapt_GR(F),
                 'base_Se': F['sensitivity'],
                 'base_Sp': F['specificity']}

    df = pd.DataFrame.from_dict(data_dict)

    # Creating the direcory and outputting the csv
    os.makedirs(results_dir, exist_ok=True)
    df.to_csv(csv_path, index=False)

def load_csv():
    # Loading the results from a csv file
    df = pd.read_csv(csv_path)
    return df


def plot(df, metric, threshold_type, threshold_value, fig=None, ax=None,
         figsize=(12,8), dpi=100, show=True, save=False, title=True):

    pivot_df = pd.pivot_table(df, values=metric, columns=['p', 'n'], index=['k'])
    mask_df = pd.pivot_table(df, values=threshold_type, columns=['p', 'n'], index=['k'])

    # Standard or inverted masking and colormap
    if threshold_type == 'E':
        mask = (mask_df.to_numpy() > threshold_value)
        comparator = '>'
    else:
        mask = (mask_df.to_numpy() < threshold_value)
        comparator = '<'
    cmap = 'coolwarm' if metric == 'E' else 'coolwarm_r'

    if fig is None:
        fig = plt.figure(figsize=figsize, dpi=dpi)
    ax = plt.gca() if ax is None else ax

    # Getting base values
    bse = df['base_Se'].unique()
    assert len(bse) == 1, 'Multiple values of base_Se in filtered_df: {}.'.format(bse)
    bsp = df['base_Sp'].unique()
    assert len(bsp) == 1, 'Multiple values of base_Sp in filtered_df: {}.'.format(bsp)
    base_Se = bse[0]
    base_Sp = bsp[0]

    title_str = (f'{metric}(A1) (Masked where {threshold_type}'
                 f'{comparator}{threshold_value}, base test '
                 f'Se={base_Se}, Sp={base_Sp})')

    if title:
        ax.set_title(title_str)

    ax = sns.heatmap(pivot_df, vmin=0., vmax=1., cmap=cmap, annot=True,
                     linewidths=0.5, cbar=False, mask=mask, linecolor='black',
                     xticklabels=['{:.2f}, {:.0f}'.format(p*100, n) for p, n in pivot_df.columns],
                     annot_kws={"size": 'smaller'}) #square=True
    ax.invert_yaxis()
    ax.set_xlabel('p [%], n')

    # Formating the interactive plot on hover coordinates
    def format_coord(x, y):
        xi, yi = np.floor(x).astype(np.int), np.floor(y).astype(np.int)
        return 'p: {:.2f}%, n: {:.0f}, k: {:.0f}, {}: {:.3f}, {}: {:.3f}'.format(
                pivot_df.columns[xi][0], pivot_df.columns[xi][1], pivot_df.index[yi],
                metric, pivot_df.values[yi][xi],
                threshold_type, mask_df.values[yi][xi])

    ax.format_coord = format_coord
    ax.set_yticklabels(ax.get_yticklabels(), rotation = 0)

    if save:
        fname = f'nonadapt_{title_str.replace(" ","_")}'
        plt.savefig(os.path.join(results_dir, '{}.pdf'.format(fname)),
                    bbox_inches = 'tight', pad_inches = 0)
    if show:
        plt.show()
