FROM r-base:3.6.3
RUN apt-get update -y && apt-get install -y --no-install-recommends libgmp-dev \
    build-essential python3.6 python3-pip python3-setuptools python3-dev git && \
    rm -rf /var/lib/apt/lists/*
RUN Rscript -e "install.packages(c('binGroup', 'jsonlite', 'optparse'))" && \
    Rscript -e "install.packages(c('repr', 'IRkernel', 'IRdisplay'), repos = c('http://irkernel.github.io/', getOption('repos')))"

WORKDIR /home/docker
COPY requirements.txt .
RUN pip3 install -r requirements.txt && \
    Rscript -e "IRkernel::installspec(user=FALSE)" && \
    jupyter nbextension enable --py --sys-prefix widgetsnbextension

COPY . .
CMD ["/bin/bash"]
